/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab01;

/**
 *
 * @author natta
 */
import java.util.Scanner;
public class Lab01 {
    
    public static void main(String[] args) {
        //mark
        Scanner kb = new Scanner(System.in);
        char ans = 'y';
        System.out.println("Welcome to OX Game");
        System.out.println("- - -");
        System.out.println("- - -");
        System.out.println("- - -");

        while(ans=='y'){
            int row;
            int col;
            char character;
    
            char[][] OX = new char[3][3];
            for(int oxrow=0; oxrow<3; oxrow++){
                for(int oxcol=0; oxcol<3; oxcol++){
                    OX[oxrow][oxcol]='-';
                }
            }

            for(int round=0; round<9; round++){
                try{
                    if(round%2==0){
                        character = 'X';
                    }else{
                        character = 'O';
                    }
                    System.out.println("Turn "+character);
                    System.out.print("Please enter your row,col : ");
                    row = kb.nextInt();
                    col = kb.nextInt();
                    for(int table1=0; table1<3; table1++){
                        for(int table2=0; table2<3; table2++){
                            if(round%2==0){
                                OX[row][col]='X';
                            }else{
                                OX[row][col]='O';
                            }
                            System.out.print(OX[table1][table2]+" ");
                        }
                        System.out.println();
                        
                    }
 
                    if(OX[0][0]=='X'&& OX[0][1]=='X'&& OX[0][2]=='X' || OX[0][0]=='O'&& OX[0][1]=='O'&& OX[0][2]=='O'){
                        if(OX[0][0]=='X'&& OX[0][1]=='X'&& OX[0][2]=='X'){
                            System.out.println("X win");
                            break;
                        }else{
                            System.out.println("O win");
                            break;
                        }
                        
                    }else if(OX[1][0]=='X'&& OX[1][1]=='X'&& OX[1][2]=='X' || OX[1][0]=='O'&& OX[1][1]=='O'&& OX[1][2]=='O'){
                        if(OX[1][0]=='X'&& OX[1][1]=='X'&& OX[1][2]=='X'){
                            System.out.println("X win");
                            break;
                        }else{
                            System.out.println("O win");
                            break;
                        }
                    }else if(OX[2][0]=='X'&& OX[2][1]=='X'&& OX[2][2]=='X' || OX[2][0]=='O'&& OX[2][1]=='O'&& OX[2][2]=='O'){
                        if(OX[2][0]=='X'&& OX[2][1]=='X'&& OX[2][2]=='X'){
                            System.out.println("X win");
                            break;
                        }else{
                            System.out.println("O win");
                            break;
                        }
                    }else if(OX[0][0]=='X'&& OX[1][0]=='X'&& OX[2][0]=='X' || OX[0][0]=='O'&& OX[1][0]=='O'&& OX[2][0]=='O'){
                        if(OX[0][0]=='X'&& OX[1][0]=='X'&& OX[2][0]=='X'){
                            System.out.println("X win");
                            break;
                        }else{
                            System.out.println("O win");
                            break;
                        }
                    }else if(OX[0][1]=='X'&& OX[1][1]=='X'&& OX[2][1]=='X' || OX[0][1]=='O'&& OX[1][1]=='O'&& OX[2][1]=='O'){
                        if(OX[0][1]=='X'&& OX[1][1]=='X'&& OX[2][1]=='X'){
                            System.out.println("X win");
                            break;
                        }else{
                            System.out.println("O win");
                            break;
                        }
                    }else if(OX[0][2]=='X'&& OX[1][2]=='X'&& OX[2][2]=='X' || OX[0][2]=='O'&& OX[1][2]=='O'&& OX[2][2]=='O'){
                        if(OX[0][2]=='X'&& OX[1][2]=='X'&& OX[2][2]=='X'){
                            System.out.println("X win");
                            break;
                        }else{
                            System.out.println("O win");
                            break;
                        }
                    }else if(OX[0][0]=='X'&& OX[1][1]=='X'&& OX[2][2]=='X' || OX[0][0]=='O'&& OX[1][1]=='O'&& OX[2][2]=='O'){
                        if(OX[0][0]=='X'&& OX[1][1]=='X'&& OX[2][2]=='X'){
                            System.out.println("X win");
                            break;
                        }else{
                            System.out.println("O win");
                            break;
                        }
                    }else if(OX[0][2]=='X'&& OX[1][1]=='X'&& OX[2][0]=='X' || OX[0][2]=='O'&& OX[1][1]=='O'&& OX[2][0]=='O'){
                        if(OX[0][2]=='X'&& OX[1][1]=='X'&& OX[2][0]=='X'){
                            System.out.println("X win");
                            break;
                        }else{
                            System.out.println("O win");
                            break;
                        }
                    }else{
                        if(round==8){
                            System.out.println("Draw"); 
                        }
                    }
                }catch(ArrayIndexOutOfBoundsException er){
                    System.out.println(er+"\nTry again");
                    round--;
                }
            }

            System.out.print("Do you want to continue(y/n) : ");
            ans = kb.next().charAt(0);

        }
    }
}
              
                    
            

